export interface ICar {
    id:number;
    Marca:string;
    Modelo: string;
    Matricula: string;
}
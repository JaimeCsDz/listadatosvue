import type { ICar } from "@/interfaces/ICar"
const CarData:ICar[] =[
    {
        id: 1,
        Marca : 'Ford',
        Modelo : 'Mustang GT',
        Matricula: 'BMV-123',
    },
    {
        id: 2,
        Marca : 'BMW',
        Modelo : 'M3 Sedán',
        Matricula: 'JCD-1702',
    },
    {
        id: 3,
        Marca : 'Chevrolet',
        Modelo : 'Aveo',
        Matricula: 'NNNM-123',
    },
    {
        id: 4,
        Marca : 'Nissan',
        Modelo : 'Versa',
        Matricula: 'VSR-1234',
    },
    {
        id: 5,
        Marca : 'Volkswagen',
        Modelo : 'Golf GTI',
        Matricula: 'QWEQD-12332',
    },
    {
        id: 6,
        Marca : 'Mercedez',
        Modelo : 'AMG GT',
        Matricula: 'MNIO-12453',
    },
    {
        id: 7,
        Marca : 'Honda',
        Modelo : 'Civic Type R',
        Matricula: 'HCT-7102',
    },
    {
        id: 8,
        Marca : 'Subaru',
        Modelo : 'BRZ',
        Matricula: 'ADCXZ-0989',
    },
    {
        id: 9,
        Marca : 'Suzuki',
        Modelo : 'Swift Sport',
        Matricula: 'CADJ0-021E1',
    },
    {
        id: 10,
        Marca : 'Toyota',
        Modelo : 'Supra V12',
        Matricula: 'QWQQW-1232',
    }
];

export default CarData